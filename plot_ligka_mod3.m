function data = plot_ligka_mod3(varargin)
% Function to plot continuum from ligka mod 3
% 
% INPUT
% norm (bool) [NOT YET IMPLEMENTED]: if 1, will normalize to the on-axis alfven frequency
% low_growthrate(float): minimum of the growthrate to plot
%
%

%% read
if ~exist('pathname', 'var')
  [~,pathname] = uigetfile([{'*spectrum*'},{'*spectrum* files'};...
                    {'*'},{'All files'}],'Select a response file'); 
end
file_list=dir(pathname);
% find n number
nn=file_list(end).name;
nn=split(nn, '_');nn=nn{end-1};
n=str2double(nn(end));

%% Parse and check arguments
p = inputParser;
% Required
addParameter(p,'oma0', 1, @isnumeric);
addParameter(p,'low_growthrate', -10, @isnumeric);
parse(p, varargin{:})
p = p.Results;

%% Set figure for plot
figure('Name', sprintf('LIGKA mod 3 - REAL part'), 'Position' ,[100, 100, 800,800]);
ax1=subplot(1,1,1); hold(ax1, 'on');
figure('Name', sprintf('LIGKA mod 3 - IMG part'), 'Position' ,[100, 100, 800,800]);
ax2=subplot(1,1,1); hold(ax2, 'on');
xlabel(ax1,'\rho_\psi'); ylabel(ax1,'\omega/\omega_0'); 
xlabel(ax2,'\gamma/\omega'); ylabel(ax2,'\omega/\omega_0');
for i=1:numel(file_list)-2
  %% read
  fname=[file_list(i+2).folder, '/', file_list(i+2).name];
  dd=dlmread(fname);
  ind = dd(:,3)>p.low_growthrate;
  if ~any(ind)
    fprintf('No Modes with growthrate>%.2f \n', p.low_growthrate);
  end
  command=sprintf('data.n%i_m%i = dd;',n,i); fprintf('Evaluating %s \n', command);
  eval(command);
  
  %% plot
  plot(ax1, dd(~ind,1), p.oma0*dd(~ind,2), 'x', 'DisplayName', sprintf('m=%s', file_list(i+2).name(end)));
  plot(ax1, dd(ind,1), p.oma0*dd(ind,2), 'o', 'DisplayName', sprintf('m=%s', file_list(i+2).name(end)));
  grid(ax1, 'on');
  plot(ax2, dd(~ind,3), p.oma0*dd(~ind,2), 'x', 'DisplayName', sprintf('m=%s', file_list(i+2).name(end)));
  plot(ax2, dd(ind,3), p.oma0*dd(ind,2), 'o', 'DisplayName', sprintf('m=%s', file_list(i+2).name(end)));
  grid(ax2, 'on');
  box(ax2, 'on');
  if p.low_growthrate~=-10 & any(ind)
    plot(ax2, [1,1]*p.low_growthrate, [min(dd(ind,2)), max(dd(ind,2))]*p.oma0, 'k')
  end
end
title(ax1,sprintf('%s', pathname), 'Interpreter', 'none'); 
title(ax2,sprintf('%s', pathname), 'Interpreter', 'none'); 
box(ax1,'on'); 
box(ax2,'on'); 
legend()
end