function data = plot_ligka_mod5(data)
%% plot of ligka mod 5
% This is only a wrapper for plot_ligka_mod4, which does similar things

data = plot_ligka_mod4(data);
ff=gcf();
ff.Name='LIGKA mod 5';

end