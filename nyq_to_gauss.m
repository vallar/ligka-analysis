function gauss = nyq_to_gauss(nyq, varargin)
% takes a nyquist array and converts it to gaussian-like array
% converted from mode5_from_nyq.py


%% Parse and check arguments
p = inputParser;
% Required
addRequired(p,'nyq', @isnumeric);
% Optional
addParameter(p,'nsidebands',10, @isnumeric);
addParameter(p,'eps',0.3, @isnumeric);
addParameter(p,'odd',1, @isnumeric);


parse(p, nyq, varargin{:})
p = p.Results;
nyq=p.nyq;

%% initializing array
sm = p.eps^2 * ones(1,4);
aa = zeros(1,4);
r0 = zeros(1,4);
sigma = zeros(1,4);
m = zeros(1,4);
gauss = [];
mref = nyq(4);
%% looping over sidebands
for ll=1:p.nsidebands+1
  ll=ll-1;
  m_next = mref + ll;
  m_prev = mref - ll;
  m(1) = m_next; m(2) = m_next+1;
  m(3) = m_prev; m(3) = m_prev+1;
  
  if ll==0
    aa(1) = 1.0;
    aa(2) = 1.0;
    r0(1) = nyq(1) - abs(nyq(31) - nyq(1))*sm(1);
    r0(2) = nyq(1) - abs(nyq(30) - nyq(1))*sm(2);
    sigma(1) = nyq(9);
    sigma(2) = nyq(9);   
  else
    aa(1) = 1.0 * ...
      abs((1.0 - abs(nyq(5) - nyq(32+(ll-1)*4)))) * ...
      abs(nyq(30+(ll-1+1)*4) - nyq(30+(ll-1)*4));
    aa(2) = 1.0 * ...
      abs((1.0 - abs(nyq(5) - nyq(32+(ll-1)*4)))) * ...
      abs(nyq(30+(ll-1+1)*4) - nyq(30+(ll-1)*4));
    
    r0(1) = nyq(30+(ll-1)*4) - ...
      abs(nyq(30+(ll-1+1)*4) - nyq(30+(ll-1)*4))*sm(1);
    r0(2) = nyq(30+(ll-1)*4) - ...
      abs(nyq(30+(ll-1+1)*4) - nyq(30+(ll-1)*4))*sm(2);
    sigma(1) = nyq(30+(ll-1)*4)/m_next;
    sigma(2) = nyq(30+(ll-1)*4)/m_next;
    
    aa(3) = 1.0 * ...
      abs((1.0 - abs(nyq(5) - nyq(33+(ll-1)*4)))) * ...
      abs(nyq(31+(ll-1+1)*4) - nyq(31+(ll-1)*4));
    aa(4) = 1.0 * ...
      abs((1.0 - abs(nyq(5) - nyq(33+(ll-1)*4)))) * ...
      abs(nyq(31+(ll-1+1)*4) - nyq(31+(ll-1)*4));
    
    r0(3) = nyq(31+(ll-1)*4) - ...
      abs(nyq(31+(ll-1+1)*4) - nyq(31+(ll-1)*4))*sm(3);
    r0(4) = nyq(31+(ll-1)*4) - ...
      abs(nyq(31+(ll-1+1)*4) - nyq(31+(ll-1)*4))*sm(3);
    if m_prev>0
      sigma(3) = nyq(31+(ll-1)*4)/m_prev;
      sigma(4) = nyq(31+(ll-1)*4)/m_prev;
    else
      sigma(3) = 0;
      sigma(4) = 0;      
    end
    if p.odd
%      a(1::2) = a(1::2)*-1;
    end
%    iharm, gauss_line in enumerate(zip(aa, r0, sigma, m, dummy, dummy)):
    for ii = 1:numel(aa)
      if ll == 0 && ii > 2
        continue
      else
        gauss_line = [aa(ii), r0(ii), sigma(ii), m(ii), ll, ii];
        mm = gauss_line(3);
        eval(sprintf('tmp=[gauss(%i), gauss_line];', m(ii)));
        gauss(mm) = tmp;
      end
    end
  end
end


end