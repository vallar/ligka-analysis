function [data, fname] = write_ligka_mod1(data)
% Writes the mod1 data in the input structure, if any

% time points
tm = data.mhd_linear_time.toroidal_mode;
%% Read psi/phi and store them
for ii=1:numel(tm)
  dominant_m=tm{ii}.m_pol_dominant;
  rho = tm{ii}.plasma.grid.dim1;
  psi = tm{ii}.plasma.psi_potential_perturbed.real;
  phi = tm{ii}.plasma.phi_potential_perturbed.real;
  kk=1;
  for jj=1:numel(tm{1}.plasma.grid.dim2)
      if tm{1}.plasma.grid.dim2(jj)<dominant_m-1 || tm{1}.plasma.grid.dim2(jj)>dominant_m+1
          continue
      end
      phi_to_save(:,kk) = phi(:, jj); 
      psi_to_save(:,kk) = psi(:, jj); 
      kk = kk+1;
  end
end
data_psi = [rho, psi_to_save];
data_phi = [rho, phi_to_save];

%% Create header
header = sprintf('#%s\n# shot/run %i/%i\n# nmodes = %i\t! number of poloidal harmonics \n# nrad = %i \t! number of radial points \n', ...
    datetime, data.input.shot, data.input.rrun, 3, numel(rho));
header = sprintf('%s# n = %i \t ! toroidal mode number \n# m = %i %i %i \t ! poloidal mode number(s) \n# f0 = %.3f kHz\t! Alfven frequency on axis\n# f_mode = %.3fkHz\t!mode frequency\n', ...
    header, tm{1}.n_tor, [tm{1}.m_pol_dominant-1, tm{1}.m_pol_dominant,tm{1}.m_pol_dominant+1], 1e-3*data.oma0,1e-3*data.omega_tae./(2*pi));
header = [header, sprintf('# rho_psi\t m=dominant-1\t m=dominant\t m=dominant+1')];

%% Store psi data
fname = sprintf('data_mod1_psi_%i_%i.dat', data.input.shot, data.input.rrun);
fid = fopen(fname, 'a+');
fprintf(fid, header);
writematrix(data_psi,fname,'WriteMode','append', 'Delimiter', 'tab')
fprintf('Psi data written in %s \n', fname);
%% Store psi data
fname = sprintf('data_mod1_phi_%i_%i.dat', data.input.shot, data.input.rrun);
fid = fopen(fname, 'a+');
fprintf(fid, header);
writematrix(data_phi,fname,'WriteMode','append', 'Delimiter', 'tab')
fprintf('Phi data written in %s \n', fname);

end