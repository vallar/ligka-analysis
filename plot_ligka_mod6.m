function data = plot_ligka_mod6(norm, varargin)
% Function to plot continuum 
% 
% INPUT
% norm (bool): if 1, will normalize to the on-axis alfven frequency
% OPTIONAL
% fname(str): filename to read

%% input parsing
p = inputParser;
% Required
addRequired(p,'norm', @isnumeric);
% Optional
addParameter(p,'fname','', @ischar);

parse(p, norm,varargin{:})
p = p.Results;
data.input=p;
%% read
try
  data=dlmread(p.fname);
catch
  [fname,pathname] = uigetfile([{'*spectrum*'},{'*spectrum* files'};...
                    {'*mod6*'},{'*mod6* files'};
                    {'*'},{'All files'}],'Select a response file');
  fnamefull = fullfile(pathname, fname);
  data=dlmread(fnamefull);
  p.fname=fnamefull;
end
ind_toplot = 2;
oma_norm =1;
if norm~=1
  ind_toplot=4;
  if data(1, ind_toplot)==data(end, ind_toplot)
      ind_toplot=2;
      oma_norm = data(1,4);
  end
end
%% plot
figure('Name', 'Ligka continuum - reduced MHD', 'Position' ,[100, 100, 800,800]);
ax1=subplot(1,1,1);
plot(ax1, data(:,1), data(:,ind_toplot)*oma_norm, 'kx');
grid(ax1, 'on');
xlabel(ax1,'\rho_\psi'); 
ylabel(ax1,'\omega/\omega_0'); 
title(ax1,p.fname, 'Interpreter', 'none'); 
if norm~=1
  ylabel(ax1,'\omega [kHz]'); 
end
title(ax1,sprintf('%s, \\omega_0=%3.2f kHz', p.fname, norm), 'Interpreter', 'none'); 

%% Setting output data structure
data_from_file=data;
data=struct;
data.data=data_from_file;
data.norm=oma_norm;

end