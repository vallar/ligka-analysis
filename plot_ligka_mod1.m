function data=plot_ligka_mod1(datain)
% plot ligka mode 1
% INPUT
% data: struct or int. If struct, it comes from IDS. Else, it reads the
% respfiles

%% Checks if data is coming from IDS or respfile
flag_respfile=0;
try
  tm = datain.mhd_linear_time.toroidal_mode;
  data=datain;
catch
  data=plot_mod1_respfile(datain);
  flag_respfile=1;
end
if ~flag_respfile
%% plot phi from IMAS
for ii=1:numel(tm)
  if tm{ii}.growthrate==100
    fprintf('Mode %i/%i has growthrate=%.2f \n', ii, numel(tm), tm{ii}.growthrate);
    continue
  end
  dominant_m=tm{ii}.m_pol_dominant;
  rho = tm{ii}.plasma.grid.dim1;
  psi = tm{ii}.plasma.psi_potential_perturbed.real;
  phi = tm{ii}.plasma.phi_potential_perturbed.real;
  figure; ax1=subplot(1,1,1); hold(ax1, 'on');
  figure; ax2=subplot(1,1,1); hold(ax2, 'on');
  for jj=1:numel(tm{1}.plasma.grid.dim2)
      if tm{1}.plasma.grid.dim2(jj)<dominant_m-1 || tm{1}.plasma.grid.dim2(jj)>dominant_m+1
        plot(ax1, rho, phi(:, jj), 'k:', 'HandleVisibility', 'off'); 
        plot(ax2, rho, psi(:, jj), 'k:', 'HandleVisibility', 'off');
        continue
      end
      plot(ax1, rho, phi(:, jj), 'DisplayName', sprintf('m=%d',tm{1}.plasma.grid.dim2(jj))); 
      plot(ax2, rho, psi(:, jj), 'DisplayName', sprintf('m=%d',tm{1}.plasma.grid.dim2(jj))); 
  end
  xlabel(ax1, '\rho_\psi');ylabel(ax1, '\phi');
  xlabel(ax2, '\rho_\psi');ylabel(ax2, '\psi');
  legend(ax1, 'show'); legend(ax2, 'show');
  grid(ax1,'on');  grid(ax2,'on'); 
  box(ax1,'on');  box(ax2,'on'); 

  try
    ttitle=sprintf('%i/%i,t=%.2f s,(n,m)=(%i,%i), \\omega=%.2f kHz, \\gamma/\\omega=%.2f', ...
        datain.input.shot, datain.input.rrun, data.time,tm{ii}.n_tor, tm{ii}.m_pol_dominant, ...
        datain.mhd_linear_time.toroidal_mode{1}.frequency*1e-3, data.growthrate./data.frequency);
    title(ax1, ttitle); title(ax2, ttitle);
  catch
    fprintf('No title to display \n');
  end
end
else
  data=plot_mod1_EFfile(data);
end
end

function data=plot_mod1_respfile(n)
% plot ligka 1 responsefile
%
% INPUT
% n (int): n number of the mode to compute response
% OUTPUT
% data(struct): structure with the name of file, frequency

%% read
fnamefull = sprintf('resp_n%i.out', n);
try
  resp=dlmread(fnamefull);
catch
  [fname,pathname] = uigetfile([{'*resp*'},{'*resp* files'}; ...
                    {'*'},{'All files'}],'Select a response file');
  olddir=pwd;
  cd(pathname);
  fnamefull = fullfile(pathname, fname);
  resp = dlmread(fnamefull);
  cd(olddir)
end
figure;
plot(resp(:,1), resp(:,2), 'kx'); xlabel('\omega/\omega_0'); ylabel('Response');
title(sprintf('n=%i', n)); grid on;
[peak_omega,~]=ginput();

filenames = dir; filenames={filenames.name};
filenames = filenames(contains(filenames, sprintf('EF_phi_n%i',n)));
% Exclude "fig" and "png" files
% filenames = split(filenames, '.');

freqs = split(filenames, '_');
freqs = {freqs{1,:,4}}; 
freqs=str2double(freqs);
for i=1:numel(peak_omega)
  [~,ind(i)]=min(abs(freqs-peak_omega(i)));
end
data.peak_omega_chosen = peak_omega;
data.peak_omega = freqs(ind);
data.file_peak_omega = {filenames{ind}};
try
  data.pathname=pathname;
catch
  data.pathname=pwd;
end
end


function data=plot_mod1_EFfile(data)
% plot ligka 1 EF_file
%
% INPUT
% data(struct): structure with the name of file and frequency to read
% OUTPUT
% data(struct): structure copy of input, with in addition the rho and
% amplitude of the modes
olddir=pwd;
cd(data.pathname);
for i=1:numel(data.file_peak_omega)
  % read header
  hdr = readlines(data.file_peak_omega{i});
  hh=split(hdr{1});
  num_r = str2double(hh{2});
  num_m = str2double(hh{3});
  n = str2double(hh{4});
  hh=split(hdr{3}); m_range = str2double(hh(2:end));
  fprintf('Reading %s \n', data.file_peak_omega{i});
  try
    data_EF=readtable(data.file_peak_omega{i}, 'HeaderLines', 3, 'FileType', 'text');
    data.EF{i}=table2array(data_EF);
  catch
    data_EF=readtable(data.file_peak_omega{i}, 'HeaderLines', 2, 'FileType', 'text');
    data.EF{i}=table2array(data_EF);    
  end
end

for i=1:numel(data.file_peak_omega)
  figure('Name', sprintf('%s, set to 0',data.file_peak_omega{i})); hold on;
  dd=data.EF{i};
  for jj=0:(numel(m_range)-1)
  plot(dd(num_r*jj+1:num_r*(jj+1),1), dd(num_r*jj+1:num_r*(jj+1),2), 'DisplayName', num2str(m_range(jj+1)));
  end
  xlabel('\rho_\psi'); ylabel('AU');
  title(sprintf('n=%i', n)); legend show;
  grid on; box on;
end

for i=1:numel(data.file_peak_omega)
  figure('Name', data.file_peak_omega{i}); hold on;
  dd=data.EF{i};
  for jj=0:(numel(m_range)-1)
  plot(dd(num_r*jj+1:num_r*(jj+1),1), (dd(num_r*jj+1:num_r*(jj+1),2))*0.1+data.peak_omega(i), 'DisplayName', num2str(m_range(jj+1)));
  end
  xlabel('\rho_\psi'); ylabel('AU');
  title(sprintf('n=%i', n)); legend show;
  grid on; box on;
end
cd(olddir);
end