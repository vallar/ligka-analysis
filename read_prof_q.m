function data=read_prof_q(varargin)
% function to read coreprofiles from database


%% Parse and check arguments
p = inputParser;
% Required
addParameter(p,'shot', 0, @isnumeric);
addParameter(p,'rrun', 0, @isnumeric);
addParameter(p,'occurrence', 0,@isnumeric);
addParameter(p,'doplot', 0,@isnumeric);
addParameter(p,'time', 1,@isnumeric);
addParameter(p,'ind', 1,@isnumeric);

% Optional
addParameter(p,'fname','', @ischar);

parse(p, varargin{:})
p = p.Results;
data.input=p;
%% read database
run('/home/ITER/sautero/public/matlab/startup.m')
pulseCtx = imas_open_env('ids', p.shot, p.rrun, 'vallarm', 'tcv', '3');
try
    eq = ids_get(pulseCtx, 'equilibrium',p.occurrence);
    coreprof = ids_get(pulseCtx, 'core_profiles',p.occurrence);
catch
    if p.occurrence==0
        eq = ids_get(pulseCtx, 'equilibrium');
        coreprof = ids_get(pulseCtx, 'core_profiles');
    else
        eq = ids_get(pulseCtx, sprintf('equilibrium', p.occurrence));
        coreprof = ids_get(pulseCtx, sprintf('core_profiles', p.occurrence));
    end
end
fprintf("Reading shot %i, run %i, occurrence %i \n", p.shot, p.rrun, p.occurrence);
data.eq = eq;
data.coreprof = coreprof;


%% read coreprofiles
[~,time_ind_eq]=min(abs(eq.time-p.time));
%% q
q=eq.time_slice{time_ind_eq}.profiles_1d.q;
psi=eq.time_slice{time_ind_eq}.profiles_1d.psi;
psi_norm = (psi-psi(1))./(psi(end)-psi(1));
rho_pol_norm_eq = sqrt(psi_norm);

%% ne, ni, te
[~,time_ind_prof]=min(abs(coreprof.time-p.time));
rho_pol_norm_coreprof = coreprof.profiles_1d{time_ind_prof}.grid.rho_pol_norm;
ne=coreprof.profiles_1d{time_ind_prof}.electrons.density;
te=coreprof.profiles_1d{time_ind_prof}.electrons.temperature;
ni = coreprof.profiles_1d{time_ind_prof}.ion{1}.density;
nC = (ne-ni)./6;
%% estimate Alfven speed in the core
va0 = abs(eq.vacuum_toroidal_field.b0)./sqrt(4*pi*1e-7*1.6e-27*(ni(1)*2+nC(1)*6));
oma0 = va0./(2*pi*eq.vacuum_toroidal_field.r0);
if numel(va0)>1
    data.va0  = va0(p.ind);
    data.oma0 = oma0(p.ind);
else
    data.va0 = va0;
    data.oma0 = oma0;
end
    %% assign data to structure
keys = ["q", "ne", "te", "ni"];
keys = [keys, "rho_pol_norm_eq", "rho_pol_norm_coreprof"];
for kk=keys
    eval(sprintf('data.%s = %s;', kk, kk));
end

%% plot
if p.doplot==1
    ff=figure('Position', [10, 10, 800, 500]); 
    ax1=subplot(1,2,1);
    plot(ax1, data.rho_pol_norm_eq, data.q, 'k-');
    xlabel(ax1, '\rho_{\psi}');     ylabel(ax1, 'q'); 
    grid(ax1, 'on'); box(ax1, 'on');
    ax2=subplot(1,2,2);
    hold(ax2, 'on');
    title(ax1, sprintf('#%i/%i t=%.3f s, f_0=%.1f kHz', p.shot, p.rrun, data.eq.time(time_ind_eq), data.oma0*1e-3));
    plot(ax2, data.rho_pol_norm_coreprof, data.ne*1e-19, 'k-', 'DisplayName', 'e');
    plot(ax2, data.rho_pol_norm_coreprof, data.ni*1e-19, 'r-', 'DisplayName', 'i');
    ylabel(ax2, 'n [1e-19 m^{-3}]'); 
    yyaxis right;
    plot(ax2, data.rho_pol_norm_coreprof, data.te*1e-3, 'k--', 'HandleVisibility' ,'off');
    xlabel(ax2, '\rho_{\psi}');  
    ylabel(ax2, 'T [keV]'); 
    legend(ax2, 'Location', 'best');
    grid(ax2, 'on'); box(ax2, 'on');
end
end