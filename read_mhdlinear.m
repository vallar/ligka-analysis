function data=read_mhdlinear(varargin)
% function to read mhd_linear from database


%% Parse and check arguments
p = inputParser;
% Required
addParameter(p,'shot', 0, @isnumeric);
addParameter(p,'rrun', 0, @isnumeric);
addParameter(p,'occurrence', 0,@isnumeric);
addParameter(p,'user', 'vallarm',@ischar);
addParameter(p,'machine', 'tcv',@ischar);
addParameter(p,'db_version', '3',@ischar);
addParameter(p,'ind_time', 1,@isnumeric);

% Optional
addParameter(p,'fname','', @ischar);

parse(p, varargin{:})
p = p.Results;
data.input=p;

if isempty(p.fname)
    run('/home/ITER/sautero/public/matlab/startup.m')
    pulseCtx = imas_open_env('ids', p.shot, p.rrun, p.user, p.machine, p.db_version);
    try
        mhd_linear = ids_get(pulseCtx, 'mhd_linear',p.occurrence);
    catch
        if p.occurrence==0
            mhd_linear = ids_get(pulseCtx, 'mhd_linear');
        else
            mhd_linear = ids_get(pulseCtx, sprintf('mhd_linear/%i', p.occurrence));
        end
    end
    fprintf("Reading shot %i, run %i, occurrence %i \n", p.shot, p.rrun, p.occurrence);
else
    mhd_linear = load(p.fname);
    fprintf("Reading file %s \n", p.fname);
end

%% initialise
try
  mhd_linear_time = mhd_linear.time_slice{p.ind_time};
catch
  mhd_linear_time = mhd_linear.mhd_linear_time;
end

try
  data.time = mhd_linear.time(p.ind_time);
catch
  data.time = mhd_linear_time.time;  
end
r_tae = zeros(1, numel(mhd_linear_time.toroidal_mode));
q_tae = zeros(1, numel(mhd_linear_time.toroidal_mode));
frequency = zeros(1, numel(mhd_linear_time.toroidal_mode));
growthrate = zeros(1, numel(mhd_linear_time.toroidal_mode));

omega_tae = zeros(1, numel(mhd_linear_time.toroidal_mode));
omega_tae_anal = zeros(1, numel(mhd_linear_time.toroidal_mode));
omega_tae_real = zeros(1, numel(mhd_linear_time.toroidal_mode));
omega_tae_img = zeros(1, numel(mhd_linear_time.toroidal_mode));

n_tae = zeros(1, numel(mhd_linear_time.toroidal_mode));
m_tae = zeros(1, numel(mhd_linear_time.toroidal_mode));
ae_continuum = zeros(1, numel(mhd_linear_time.toroidal_mode));

nyquist_array=[];

%% store modes properties
for i=1:numel(mhd_linear_time.toroidal_mode)
  mode = mhd_linear_time.toroidal_mode{i}; 
  frequency(i) = mode.frequency;
  growthrate(i) = mode.growthrate;
  nyquist_array = mode.plasma.velocity_perturbed.coordinate1.coefficients_real;
  if numel(nyquist_array)>0
      r_tae(i) = nyquist_array(1); %sqrt (poloidal flux normalized)
      q_tae(i) = nyquist_array(2);
      n_tae(i) = nyquist_array(3);
      m_tae(i) = nyquist_array(4);
      omega_tae_anal(i) = nyquist_array(5); %in units of omega_0
      omega_tae(i) = nyquist_array(10); % mode frequency
      oma0(i) = mode.frequency*2*pi./nyquist_array(5);
      % mode.frequency = nyquist_array(5)*oma0/(2.0d0*pi)
      omega_tae_real(i) =  nyquist_array(14);
      omega_tae_img(i) =  nyquist_array(15);

      gap_width(i) = nyquist_array(11);
      ae_continuum(i)=nyquist_array(5);
      %     ae_continuum(i)=omega_tae(i)./mode.plasma.velocity_perturbed.coordinate1.coefficients_real(10);
      %     n_tae(i) = mode.n_tor;
      %     m_tae(i) = mode.m_pol_dominant;


    %   30: lig_wrap%nyquist_scan_arr(counter,30+(ll-1)*4)=s_n_gap(ll): distance to next gap
    %   31:lig_wrap%nyquist_scan_arr(counter,31+(ll-1)*4)=s_p_gap(ll): distance to previous gap
    %   32: lig_wrap%nyquist_scan_arr(counter,32+(ll-1)*4)=wmode_n_gap(ll)
    %   33: lig_wrap%nyquist_scan_arr(counter,33+(ll-1)*4)=wmode_p_gap(ll)
      for ll = 1:numel(nyquist_array)-40
        s_n_gap(i,ll) = nyquist_array(29+ll);
        s_p_gap(i,ll) = nyquist_array(30+ll);
        wmode_n_gap(i,ll) = nyquist_array(31+ll);
        wmode_p_gap(i,ll) = nyquist_array(32+ll);
      end
  end
end

%% put to data structure
kkeys = ["growthrate", "frequency", "mhd_linear", "mhd_linear_time"];
if numel(nyquist_array)>0
    kkeys = [kkeys, "r_tae", "q_tae", "n_tae", "m_tae", "omega_tae", "oma0", "gap_width", "ae_continuum"];
    kkeys = [kkeys, "s_n_gap", "s_p_gap", "wmode_n_gap", "wmode_p_gap", "omega_tae_anal"];
end
for kk=kkeys
  eval(sprintf('data.%s=%s;', kk, kk));
end

end