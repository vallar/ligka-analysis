mod=1;

switch mod
  case mod==1
    %% mod 1
    ffolder='/home/vallar/WORK/WPTE/2021/experiments/TCV/ligka/72101';
    n=1;
    cwd = pwd;
    cd(ffolder);
    %% read resp file and choose the maximum response
    data1=plot_ligka_mod1(n);
    %% back to previous folder
    cd(cwd);

  case mod==3
    %% mod 3
    ffolder='~/RT11/2021/TCV/72101_run89/';
    olddir=pwd;
    cd(ffolder);
    data=plot_ligka_mod3(); %It will ask for a file, but the important variable is the folder
    cd(olddir);

  case mod==4
    %% mod 4
    run('/home/ITER/sautero/public/matlab/startup.m')
    shot=64949;
    run=3;
    occurrence=1;
    data=read_mhdlinear('shot', shot, 'rrun', run, 'occurrence', occurrence, 'machine', machine);
    % you can also do 
    %   data4=read_mhdlinear('fname', fname);
    % where fname is a matlab structure result of read_mhdlinear

    data=plot_ligka_mod4(data4);
  case mod==5
    %% mod 5
    run('/home/ITER/sautero/public/matlab/startup.m')
    shot=64949;
    run=3;
    occurrence=0;
    data=read_mhdlinear('shot', shot, 'rrun', run, 'occurrence', occurrence, 'machine', machine);
    % you can also do 
    %   data4=read_mhdlinear('fname', fname);
    % where fname is a matlab structure result of read_mhdlinear

    data=plot_ligka_mod5(data);
  case mod==6
      %% mod 6
      norm=1; %In this way, the plot will be normalized
      data = plot_ligka_mod6(norm); %% will ask you which file you want to read
  otherwise
    fprintf('Mod %d not implemented yet \n', mod);
    
end