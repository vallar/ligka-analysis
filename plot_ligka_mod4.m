function data = plot_ligka_mod4(data)
%% plot of Ligka 4 output
%
% INPUT
% data (struct): data coming from read_mhdlinear (i.e. IDS mhd_linear)
colors=['g','b','r','k','m','c'];

ffig=figure('Position', [100,100,1000,500], 'Name', 'LIGKA mod 4'); hold on;
ax1=subplot(1,2,1); hold(ax1, 'on');
ax2=subplot(1,2,2); hold(ax2, 'on');
for i=1:10
  iind = (data.n_tae==i & data.frequency~=0);
  if ~any(iind), continue; end
  plot(ax1, data.growthrate(iind)./data.omega_tae(iind)*100., data.frequency(iind)*1e-3, ['o', colors(i)], ...
    'DisplayName', sprintf('n=%i', i));
  plot(ax2, data.r_tae(iind), data.frequency(iind)*1e-3, ['o', colors(i)], ...
    'DisplayName', sprintf('n=%i', i));
end
[~, ordind]=sort(data.r_tae);
plot(ax2, data.r_tae(ordind), -1.*data.q_tae(ordind),'k--');
for aa=[ax1,ax2]
    legend(aa,'show', 'Location', 'best')
    grid(aa, 'on'); box(aa,'on');
end
yticklabels(ax2,[])
linkaxes([ax1,ax2], 'y');
try
  title(ax1,sprintf('shot=%i, run=%i, t=%.2f s, m=%i:%i', ...
  data.input.shot, data.input.rrun, data.mhd_linear.time, min(data.m_tae), max(data.m_tae)));
catch
  title(ax1,sprintf('m=%i:%i',  min(data.m_tae), max(data.m_tae)));
end
ylabel(ax1, '\omega [kHz]');
xlabel(ax1,'\gamma/\omega [%]'); 
xlabel(ax2, '\rho');
end
